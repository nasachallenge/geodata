package eclipse.geodata.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GeoDataController {

    @GetMapping("/test-data/{name}")
    public String helloWorld(@PathVariable("name") String name){
        return "Hello " + name;
    }
}

FROM gcr.io/distroless/java:11
WORKDIR /app

COPY build/libs/geodata-*-SNAPSHOT.jar /app/geodata.jar
ENTRYPOINT ["java", "-Xms2g", "-Xmx4g","-jar", "/app/geodata.jar"]
